:: @name exampleJourneywarHosts.cmd
:: @author PepeAmpere
:: @description Some game example hosts for JW project

@echo off

:: GLOBAL CONSTANTS
call N:\nota_dev_pack\000\hostDefault\SPADS-launchers\constants.cmd

:: THIS HOST DATA
set path=%launcherPath%
set configuration=JWstable
set login=jwHost001
set password=xxxxx
set port=5981
set autohostport=5991
set battleName=Stable journeywar
set battleDescription=Grow your fungus!
set battleSetup=1
set battlePassword=*

:: LAUNCH HOST
echo %launcherPath%
start "%configuration% - %login%" /d %launcherDir% /i %launcherPath%^
 %configuration%^
 %login%^
 %password%^
 %port%^
 %autohostport%^
 "%battleName%"^
 "%battleDescription%"^
 "%battleSetup%"^
 "%battlePassword%"

:: THIS HOST DATA
set path=%launcherPath%
set configuration=JWdev
set login=jwHost002
set password=xxxxx
set port=5982
set autohostport=5992
set battleName=Dev journeywar
set battleDescription=Centrail hub
set battleSetup=1
set battlePassword=*

:: LAUNCH HOST
echo %launcherPath%
start "%configuration% - %login%" /d %launcherDir% /i %launcherPath%^
 %configuration%^
 %login%^
 %password%^
 %port%^
 %autohostport%^
 "%battleName%"^
 "%battleDescription%"^
 "%battleSetup%"^
 "%battlePassword%"