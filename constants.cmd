:: @name constants.cmd
:: @author PepeAmpere
:: @description provides constants for all the launcher scripts

set launcherDir=N:\nota_dev_pack\000\hostDefault\SPADS\src
set launcherFile=spadsGeneral.cmd
set launcherPath=%launcherDir%\%launcherFile%

:: SUPPORTED CONFIGURATIONS

:: JWstable
set JW_STABLE_PATH=\hostDefault
set JW_STABLE_ENGINE=103.0
set JW_STABLE_GAME=Journeywar 1.0
set JW_STABLE_MAP=
set JW_STABLE_MAPS_CONFIGURATION=JWstable

:: JWdev
set JW_DEV_PATH=%JW_STABLE_PATH%
set JW_DEV_ENGINE=103.0
set JW_DEV_GAME=Journeywar beta-161102
set JW_DEV_MAP=
set JW_DEV_MAPS_CONFIGURATION=JWdev